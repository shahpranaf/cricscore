<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$time = date('r');
echo "data: The server time is: {$time}\n\n";
flush();

function curl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function get_score(){
	//$url = 'http://cricscore-api.appspot.com/csa';
	//$url = 'http://live.cricbuzz.com/livecricketscore/2015/2015_WC/BAN_IND_MAR19/commentary-push.json';
	//$url = 'http://live.cricbuzz.com/livecricketscore/2015/2015_WC/NZ_RSA_MAR24/commentary-push.json';
	$url = 'http://live.cricbuzz.com/livecricketscore/2015/2015_WC/NZ_AUS_MAR29/commentary-push.json?callback=JSON_CALLBACK';
	return curl($url);
}

//echo get_score();

?>