<!DOCTYPE html>
<!-- cricket score by pranav  -->
<html lang="en-us"  ng-app="cricapp"  ng-controller = "cricapps">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title ngCloak>{{scores.batteamname}}:{{scores.batteamdesc}}  | {{scores.bwlteamname}}:{{scores.bwlteamdesc}}  </title>
<style type="text/css">
#mainContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: xx-large;
	font-weight: bold;
	background-color: #E3F0FB;
	border-radius: 4px;
	padding: 10px;
	text-align: center;
}
.buttonStyle {
	border-radius: 4px;
	border: thin solid #F0E020;
	padding: 5px;
	background-color: #F8F094;
	font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
	font-weight: bold;
	color: #663300;
	width: 75px;
}

.buttonStyle:hover {
	border: thin solid #FFCC00;
	background-color: #FCF9D6;
	color: #996633;
	cursor: pointer;
}
.buttonStyle:active {
	border: thin solid #99CC00;
	background-color: #F5FFD2;
	color: #669900;
	cursor: pointer;
}

</style>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-sanitize.js"></script>
</head>

<body>
	<h2>{{date1}}</h2>
	<div class="container-fluid" ng-show="scores">
		<h2 class="text-primary">{{scores.team1}} VS {{scores.team2}}</h2>
		<span class="current-time-info"><strong>Match time</strong>
			<span>{{scores.preview_msg}}</span>             
		</span>
		<div class="row col-lg-8 col-md-12 col-sm-12">
			
			<h3>{{scores.batteamname}}:{{scores.batteamruns}}/{{scores.batteamwkts}}({{scores.batteamovers}})</h3>
			<!--<h3>{{scores.batteamname}}:{{scores.batteamdesc}}</h3> -->
			
			<h3>{{scores.bwlteamname}}:{{scores.bwlteamdesc}}</h3>
			<p>Curr RunRate:{{scores.curr_runrate}}</p>
			
			<table ngCloak width="100%" class="score-table table table-bordered">
				<tr>
					<th class="match-n-time-head" scope="col">
						<span class="match-status-info">{{scores.status}}</span>
					</th>
					<th>        
						<span class="current-time-info"><strong>Match time</strong>
							<span>{{scores.preview_msg}}</span>             
						</span>        
					</th>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" class="batsmen">
							<thead>
								<tr>
									<th class="th-batsmen">Batsmen</th>
									<th class="th-r">R</th>
									<th class="th-b">B</th>
									<th class="th-4s">4s</th>
									<th class="th-6s">6s</th>						
								</tr>
							</thead>
							<tbody>							
								<tr class="row1">
									<td class="batsman-1-name current-player">
											<a target="_blank" title="" href="">{{scores.batsman[0].name}}</a>
									</td>
									<td class="bold">{{scores.batsman[0].runs}}</td>
									<td>{{scores.batsman[0].balls_faced}}</td>
									<td>{{scores.batsman[0].fours}}</td>
									<td>{{scores.batsman[0].sixes}}</td>
								</tr>
					
								<tr class="row2">
									<td class="batsman-2-name">
											<a target="_blank" title="" href="">{{scores.batsman[1].name}}</a>
									</td>
									<td class="bold">{{scores.batsman[1].runs}}</td>
									<td>{{scores.batsman[1].balls_faced}}</td>
									<td>{{scores.batsman[1].fours}}</td>
									<td>{{scores.batsman[1].sixes}}</td>
								</tr>
					
							</tbody>
						</table>
					</td>		
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" class="bowlers">
							<thead>
								<tr>
									<th class="th-bowlers">Bowlers</th>
									<th class="th-o">O</th>
									<th class="th-m">M</th>
									<th class="th-r">R</th>
									<th class="th-w">W</th>								
								</tr>
							</thead>
							<tbody>							
								<tr class="row1">
									<td class="bowler-1-name current-player">
										<a target="_blank" title="" href="">{{scores.bowler[0].name}}</a>
									</td>
									<td>{{scores.bowler[0].overs}}</td>
									<td>{{scores.bowler[0].maidens}}</td>
									<td>{{scores.bowler[0].runs}}</td>
									<td>{{scores.bowler[0].wickets}}</td>
								</tr>
							
								<tr class="row2">
									<td class="bowler-2-name">
										<a target="_blank" title="" href="">{{scores.bowler[1].name}}</a>
									</td>
									<td>{{scores.bowler[1].overs}}</td>
									<td>{{scores.bowler[1].maidens}}</td>
									<td>{{scores.bowler[1].runs}}</td>
									<td>{{scores.bowler[1].wickets}}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>		
		</div><?php
		if( isset($_REQUEST['com']) && $_REQUEST['com'] ){ ?>
		<div ngCloak class="commentary row col-lg-8 col-md-12 col-sm-12">
			<div class="commentary-text col-lg-12" ng-repeat="cmt in cmts">			
				<div ng-if = "cmt.ballNbr !=0" class="over col-lg-1"><b>{{cmt.ballNbr}}</b></div>
				<div ng-if = "cmt.commtxt !=''"  ng-bind-html="cmt.commtxt" class="col-lg-11"></div>
			</div><br>		
		</div><?php
		}?>
	</div>

	
<script src="app.js"></script>
 <script src="services.js"></script> 
<script src="controller.js"></script>	
</body>
</html>
